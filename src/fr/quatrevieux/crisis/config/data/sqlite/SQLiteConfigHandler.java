/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.data.sqlite;

import fr.quatrevieux.crisis.config.data.ConfigHandler;
import fr.quatrevieux.crisis.config.data.ConfigNode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author vincent
 */
public class SQLiteConfigHandler implements ConfigHandler{
    final private Connection connection;
    final private String tableName;
    final private String attrParent;
    final private String attrValue;
    final private String attrName;
    final private String attrRowId;
    
    private ConfigNode root;

    public SQLiteConfigHandler(Connection connection, String tableName, String attrParent, String attrValue, String attrName, String attrRowId) throws SQLException {
        this.connection = connection;
        this.tableName = tableName;
        this.attrParent = attrParent;
        this.attrValue = attrValue;
        this.attrName = attrName;
        this.attrRowId = attrRowId;
        createTable();
    }
    
    private void createTable() throws SQLException{
        connection.createStatement().execute(
            "CREATE TABLE IF NOT EXISTS " + tableName + "("
                + attrRowId + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + attrName + " TEXT,"
                + attrParent + " INTEGER,"
                + attrValue + " TEXT"
            + ")"
        );
    }

    @Override
    public ConfigNode getRoot() {
        if(root != null)
            return root;
        
        List<SQLiteConfigNode> nodes = new ArrayList<>();
        
        try(ResultSet RS = connection.createStatement().executeQuery("SELECT * FROM " + tableName + " WHERE " + attrParent + " IS NULL")){
            while(RS.next())
                nodes.add(createByRS(RS, null));
        }catch(SQLException e){
            e.printStackTrace();
        }
        
        root = new SQLiteRootNode(this, nodes);
        return root;
    }
    
    private SQLiteConfigNode createByRS(ResultSet RS, SQLiteConfigNode parent) throws SQLException{
        Collection<SQLiteConfigNode> childs = new ArrayList<>();
        SQLiteConfigNode node = new SQLiteConfigNode(
            RS.getInt(attrRowId), 
            this, 
            parent, 
            RS.getString(attrName), 
            RS.getString(attrValue), 
            childs
        );
        childs.addAll(findNodes(node));
        return node;
    }
    
    private Collection<SQLiteConfigNode> findNodes(SQLiteConfigNode parent){
        Collection<SQLiteConfigNode> nodes = new ArrayList<>();
        
        try(PreparedStatement stmt = connection.prepareStatement("SELECT * FROM " + tableName + " WHERE " + attrParent + " = ?")){
            stmt.setInt(1, parent.rowid);
            
            try(ResultSet RS = stmt.executeQuery()){
                while(RS.next()){
                    nodes.add(createByRS(RS, parent));
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        
        return nodes;
    }

    @Override
    public void saveNode(ConfigNode node) {
        SQLiteConfigNode n = (SQLiteConfigNode) node;
        try(PreparedStatement stmt = connection.prepareStatement("UPDATE " + tableName + " SET " + attrValue + " = ? WHERE " + attrRowId + " = ?")){
            stmt.setString(1, node.getValue());
            stmt.setInt(2, n.rowid);
            stmt.execute();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    SQLiteConfigNode insertNode(SQLiteConfigNode node){
        try(PreparedStatement stmt = connection.prepareStatement("INSERT INTO " + tableName + "(" + attrName + "," + attrParent + "," + attrValue + ") VALUES(?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS)){
            stmt.setString(1, node.getName());
            
            if(node.getParent() == null)
                stmt.setNull(2, Types.INTEGER);
            else
                stmt.setInt(2, ((SQLiteConfigNode)node.getParent()).rowid);
            
            stmt.setString(3, node.getValue());
            stmt.execute();
            
            ResultSet RS = stmt.getGeneratedKeys();
            
            if(!RS.next())
                return null;
            
            return new SQLiteConfigNode(RS.getInt(1), this, node.getParent(), node.getName(), node.getValue(), node.nodes);
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
