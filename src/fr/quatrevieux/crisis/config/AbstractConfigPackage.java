/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config;

import java.lang.reflect.Field;
import fr.quatrevieux.crisis.config.data.ConfigNode;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
public class AbstractConfigPackage implements ConfigPackage{
    @Override
    public void parse(ConfigNode node) throws ConfigException {
        for (Field field : getClass().getDeclaredFields()) {
            field.setAccessible(true);
            
            if(ConfigItem.class.isAssignableFrom(field.getType())){ //the field is a config item
                String name = field.getName();
                
                ConfigNode valueNode = node.getFirstNodeByName(name);
                
                if(valueNode == null)
                    valueNode = node.createEmptyNode(name);
                
                String defValue = "";
                
                if(field.isAnnotationPresent(DefaultValue.class)){
                    defValue = field.getAnnotation(DefaultValue.class).value();
                }
                
                try {
                    Object item = field.getType().getConstructors()[0].newInstance(new Object[]{valueNode, defValue});
                    field.set(this, item);
                } catch (Exception ex) {
                    throw new ConfigException("Invalid constructor for ConfigItem " + field.getType().getSimpleName(), ex);
                }
                
            }else if(ConfigPackage.class.isAssignableFrom(field.getType())){ //the field is a config package
                String name = field.getName();
                
                ConfigNode valueNode = node.getFirstNodeByName(name);
                
                try{
                    if(field.get(this) == null){
                        try {
                            Object item = field.getType().newInstance();
                            field.set(this, item);
                        } catch (Exception ex) {
                            throw new ConfigException("Can use default contructor on " + field.getDeclaringClass().getSimpleName(), ex);
                        }
                    }
                    
                    ConfigPackage obj = (ConfigPackage) field.get(this);
                    obj.parse(valueNode);
                }catch(Exception e){
                    throw new ConfigException("Error during parsing ConfigPackage", e);
                }
            }
        }
    }

    
}
