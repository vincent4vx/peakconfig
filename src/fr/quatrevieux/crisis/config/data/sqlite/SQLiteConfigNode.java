/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.data.sqlite;

import fr.quatrevieux.crisis.config.data.ConfigHandler;
import fr.quatrevieux.crisis.config.data.ConfigNode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author vincent
 */
public class SQLiteConfigNode implements ConfigNode{
    final int rowid;
    final protected SQLiteConfigHandler handler;
    final private ConfigNode parent;
    final private String name;
    private String value;
    final Collection<SQLiteConfigNode> nodes;

    public SQLiteConfigNode(int rowid, SQLiteConfigHandler handler, ConfigNode parent, String name, String value, Collection<SQLiteConfigNode> nodes) {
        this.rowid = rowid;
        this.handler = handler;
        this.parent = parent;
        this.name = name;
        this.value = value;
        this.nodes = nodes;
    }

    @Override
    public ConfigHandler getHandler() {
        return handler;
    }

    @Override
    public ConfigNode getParent() {
        return parent;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public Collection<ConfigNode> getNodes() {
        return Collections.unmodifiableCollection(nodes);
    }

    @Override
    public void setValue(String value) {
        this.value = value;
        handler.saveNode(this);
    }

    @Override
    public ConfigNode getFirstNodeByName(String name) {
        for (ConfigNode node : nodes) {
            if(node.getName().equals(name))
                return node;
        }
        
        return null;
    }

    @Override
    public void addNode(ConfigNode node) {
        nodes.add(handler.insertNode((SQLiteConfigNode)node));
    }

    @Override
    public ConfigNode createEmptyNode(String name) {
        SQLiteConfigNode node = new SQLiteConfigNode(-1, handler, this, name, null, new ArrayList<>());
        return handler.insertNode(node);
    }
}
