/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.data;

import fr.quatrevieux.crisis.config.data.internal.ConfigNodeImpl;
import java.util.Collection;

/**
 *
 * @author vincent
 */
public class DefaultConfigNodeFactory implements ConfigNodeFactory{

    @Override
    public ConfigNode createNode(ConfigHandler handler, ConfigNode parent, String name, String value, Collection<ConfigNode> childs) {
        return new ConfigNodeImpl(handler, parent, name, value, childs);
    }
}
