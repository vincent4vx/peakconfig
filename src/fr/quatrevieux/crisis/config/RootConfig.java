/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config;

import java.util.HashMap;
import java.util.Map;
import fr.quatrevieux.crisis.config.data.ConfigNode;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
public class RootConfig{
    final private ConfigNode config;
    final private Map<Class<? extends ConfigPackage>, ConfigPackage> packages = new HashMap<>();

    public RootConfig(ConfigNode config) {
        this.config = config;
    }
    
    public<T extends ConfigPackage> T getPackage(Class<T> clazz) throws InstantiationException, IllegalAccessException, ConfigException{
        if(!packages.containsKey(clazz)){
            T cp = clazz.newInstance();
            ConfigNode localConf = config.getFirstNodeByName(clazz.getSimpleName());
            
            if(localConf == null){
                localConf = config.createEmptyNode(clazz.getSimpleName());
            }
            
            cp.parse(localConf);
            packages.put(clazz, cp);
            return cp;
        }
        
        return (T)packages.get(clazz);
    }
}
