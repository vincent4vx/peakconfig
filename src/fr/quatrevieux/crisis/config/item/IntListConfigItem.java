/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.item;

import java.util.ArrayList;
import java.util.List;
import fr.quatrevieux.crisis.config.AbstractConfigItem;
import fr.quatrevieux.crisis.config.data.ConfigNode;
import org.peakemu.common.util.StringUtil;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
public class IntListConfigItem extends AbstractConfigItem<List<Integer>>{

    public IntListConfigItem(ConfigNode node, String defaultValue) {
        super(node, defaultValue);
    }

    @Override
    protected List<Integer> parseValue(String value) {
        List<Integer> list = new ArrayList<>();
        
        for(String sElem : StringUtil.split(value, ",")){
            sElem = sElem.trim();
            
            if(sElem.isEmpty())
                continue;
            
            list.add(Integer.parseInt(sElem));
        }
        
        return list;
    }

}
