package fr.quatrevieux.crisis.config.data.internal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import fr.quatrevieux.crisis.config.data.ConfigHandler;
import java.util.ArrayList;
import java.util.Collection;
import fr.quatrevieux.crisis.config.data.ConfigNode;
import java.util.Collections;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
public class ConfigNodeImpl implements ConfigNode{
    final private ConfigHandler handler;
    final private ConfigNode parent;
    final private String name;
    private String value;
    final private Collection<ConfigNode> nodes;

    public ConfigNodeImpl(ConfigHandler handler, ConfigNode parent, String name, String value, Collection<ConfigNode> nodes) {
        this.handler = handler;
        this.parent = parent;
        this.name = name;
        this.value = value;
        this.nodes = nodes;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Collection<ConfigNode> getNodes() {
        return Collections.unmodifiableCollection(nodes);
    }

    @Override
    public ConfigNode getFirstNodeByName(String name) {
        for (ConfigNode node : nodes) {
            if(node.getName().equals(name))
                return node;
        }
        
        return createEmptyNode(name);
    }

    @Override
    public ConfigHandler getHandler() {
        return handler;
    }

    @Override
    public ConfigNode getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return "ConfigNodeImpl{" + "name=" + name + ", value=" + value + ", nodes=" + nodes + '}';
    }
    
    @Override
    public ConfigNode createEmptyNode(String name){
        ConfigNode node = new ConfigNodeImpl(handler, parent, name, "", new ArrayList<ConfigNode>());
        addNode(node);
        return node;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public void addNode(ConfigNode node) {
        nodes.add(node);
    }
}
