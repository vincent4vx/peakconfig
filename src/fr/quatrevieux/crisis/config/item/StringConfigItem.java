/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.item;

import fr.quatrevieux.crisis.config.AbstractConfigItem;
import fr.quatrevieux.crisis.config.data.ConfigNode;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
public class StringConfigItem extends AbstractConfigItem<String>{

    public StringConfigItem(ConfigNode node, String defaultValue) {
        super(node, defaultValue);
    }

    @Override
    protected String parseValue(String value) {
        return value;
    }
    
}
