/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.data.xml;

import fr.quatrevieux.crisis.config.data.ConfigHandler;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import fr.quatrevieux.crisis.config.data.ConfigNode;
import fr.quatrevieux.crisis.config.data.ConfigNodeFactory;
import fr.quatrevieux.crisis.config.data.DefaultConfigNodeFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
public class ConfigXMLHandler extends DefaultHandler implements ConfigHandler{
    final private Deque<ConfigNode> nodesStack = new ArrayDeque<>();
    
    private ConfigNode root;
    
    final private String configFile;
    final private ConfigNodeFactory factory;

    public ConfigXMLHandler(String configFile) throws IOException, SAXException, ParserConfigurationException {
        this(configFile, new DefaultConfigNodeFactory());
    }

    public ConfigXMLHandler(String configFile, ConfigNodeFactory factory) throws IOException, SAXException, ParserConfigurationException {
        this.configFile = configFile;
        this.factory = factory;
        parse();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        nodesStack.getLast().setValue(new String(ch, start, length));
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        nodesStack.removeLast();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(nodesStack.isEmpty()){
            root = factory.createRootNode(this, localName);
            nodesStack.addLast(root);
        }else{
            ConfigNode node = nodesStack.getLast().createEmptyNode(localName);
            nodesStack.addLast(node);
        }
    }

    public void parse() throws IOException, SAXException, ParserConfigurationException{
        XMLReader reader = XMLReaderFactory.createXMLReader();
        reader.setContentHandler(this);
        reader.parse(configFile);
    }
    
    @Override
    public ConfigNode getRoot(){
        return root;
    }

    @Override
    public void saveNode(ConfigNode node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
