/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.data;

/**
 *
 * @author vincent
 */
public interface ConfigHandler {
    public ConfigNode getRoot();
    public void saveNode(ConfigNode node);
}
