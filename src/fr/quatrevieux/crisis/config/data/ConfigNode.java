/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.data;

import java.util.Collection;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
public interface ConfigNode {
    public String getValue();
    public void setValue(String value);
    public String getName();
    public Collection<ConfigNode> getNodes();
    public ConfigNode getFirstNodeByName(String name);
    public void addNode(ConfigNode node);
    public ConfigNode getParent();
    public ConfigHandler getHandler();
    public ConfigNode createEmptyNode(String name);
}
