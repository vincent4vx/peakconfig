/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config;

import fr.quatrevieux.crisis.config.data.ConfigNode;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
abstract public class AbstractConfigItem<T> implements ConfigItem<T>{
    final private ConfigNode node;
    private T value;
    final private T defaultValue;

    public AbstractConfigItem(ConfigNode node, String defaultValue) {
        this.node = node;
        this.defaultValue = parseValue(defaultValue.trim());
        String sVal = node.getValue() == null ? "" : node.getValue().trim();
        
        if(sVal.isEmpty())
            this.value = getDefault();
        else
            this.value = parseValue(sVal);
    }
    
    abstract protected T parseValue(String value);
    
    protected String serializeValue(){
        return value.toString();
    }

    @Override
    public T getValue() {
        return value;
    }
    
    public void setValue(T value){
        this.value = value;
        node.setValue(serializeValue());
    }

    @Override
    public T getDefault() {
        return defaultValue;
    }
    
}
