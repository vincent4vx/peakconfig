/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.item;

import fr.quatrevieux.crisis.config.AbstractConfigItem;
import fr.quatrevieux.crisis.config.data.ConfigNode;

/**
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
public class LongConfigItem extends AbstractConfigItem<Long>{

    public LongConfigItem(ConfigNode node, String defaultValue) {
        super(node, defaultValue);
    }

    @Override
    protected Long parseValue(String value) {
        return Long.parseLong(value);
    }
    
}
