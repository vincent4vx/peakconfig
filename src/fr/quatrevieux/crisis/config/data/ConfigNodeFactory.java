/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.data;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author vincent
 */
public interface ConfigNodeFactory {
    public ConfigNode createNode(ConfigHandler handler, ConfigNode parent, String name, String value, Collection<ConfigNode> childs);
    default public ConfigNode createNode(ConfigHandler handler, ConfigNode parent, String name){
        return createNode(handler, parent, name, null, new ArrayList<>());
    }
    default public ConfigNode createRootNode(ConfigHandler handler, String name){
        return createNode(handler, null, name);
    }
}
