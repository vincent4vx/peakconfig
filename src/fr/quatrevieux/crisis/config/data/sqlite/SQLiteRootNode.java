/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.quatrevieux.crisis.config.data.sqlite;

import fr.quatrevieux.crisis.config.data.ConfigNode;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author vincent
 */
public class SQLiteRootNode extends SQLiteConfigNode{

    public SQLiteRootNode(SQLiteConfigHandler handler, Collection<SQLiteConfigNode> nodes) {
        super(-1, handler, null, null, null, nodes);
    }

    @Override
    public void setValue(String value) {
        throw new IllegalAccessError("Can't modify root");
    }
    
    @Override
    public ConfigNode createEmptyNode(String name) {
        SQLiteConfigNode node = new SQLiteConfigNode(-1, handler, null, name, null, new ArrayList<>());
        return handler.insertNode(node);
    }
}
